ARG RUST_VERSION=rust:slim
FROM $RUST_VERSION

RUN apt update
RUN apt install -yq libssl-dev
RUN apt install -yq pkg-config
  
RUN cargo --version
RUN cargo install aragog_cli -qf
